﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MapMenuObject : MonoBehaviour
{
    // MAP OBJECT ELEMENTS
    [Header("MAP OBJECT ELEMENTS")]
    [SerializeField] public GameObject mapButtonObject = null;

    [SerializeField] public Image mapImage = null;

    [SerializeField] public TextMeshProUGUI mapText = null;






    // BASE FUNCTIONS
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per graphic frame
    void Update()
    {
        
    }
}
